package com.example.alimurad.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.*;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.*;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {


    Button b[][];
    TextView textView;
    TextView info;

    Button restart;

    int xSize = 3;
    int ySize = 3;
    Game_Board board;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createGame();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuItem item = menu.add("New Game");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        //createGame();
        return true;
    }


    private  void restart()
    {
        info.setText("game has started");
        textView.setText(" ");

        for(int i=0; i<xSize; i++){
            for(int j =0; j <ySize; j++)
            {

                if(!b[i][j].isEnabled()) {
                    b[i][j].setText(" ");
                    b[i][j].setEnabled(true);
                }
            }
        }

        board = new Game_Board(xSize,ySize);
    }

    private void createGame()
    {
        b = new Button[xSize][ySize];
        //c = new int[xSize][ySize];

        textView = (TextView) findViewById(R.id.dialogue);
        info = (TextView) findViewById(R.id.info);

        info.setText("game has started");

        //buttons = new Button[xSize * ySize];
        //buttonsValue = new String[xSize * ySize];

        b[0][2] = (Button) findViewById(R.id.one);
        b[0][1] = (Button) findViewById(R.id.two);
        b[0][0] = (Button) findViewById(R.id.three);


        b[1][2] = (Button) findViewById(R.id.four);
        b[1][1] = (Button) findViewById(R.id.five);
        b[1][0] = (Button) findViewById(R.id.six);


        b[2][2] = (Button) findViewById(R.id.seven);
        b[2][1] = (Button) findViewById(R.id.eight);
        b[2][0] = (Button) findViewById(R.id.nine);



        restart = (Button) findViewById(R.id.restart);
        restart.setText("New Game");
        restart.setTextSize(12);


        textView.setText(" ");


        // add the click listeners for each button
        for(int i=0; i<xSize; i++){
            for(int j =0; j <ySize; j++)
            {
                b[i][j].setOnClickListener(new MyClickListener(i,j));
                if(!b[i][j].isEnabled()) {
                    b[i][j].setText(" ");
                    b[i][j].setEnabled(true);
                }
            }
        }

//
        restart.setOnClickListener(new MyClickListener(10,10));

        board  = new Game_Board(xSize,ySize);





    }


    class MyClickListener implements View.OnClickListener {
        int x;
        int y;



        public MyClickListener(int x, int y) {
            this.x = x;
            this.y = y;
        }


        public void onClick(View view)
        {

            if(restart.isEnabled() && x == 10)
            {
                restart();
            }
            else if (b[x][y].isEnabled())
            {

                if(!board.isGameOver())
                {
                    b[x][y].setEnabled(false);
                    b[x][y].setText("O");

                    textView.setText("");

                    info.append("\nPLAYER TURN: " + x + "," + y);
                    BoardLocation bl = new BoardLocation(x, y);
                    board.placeAMove(bl, 2);
                    if (board.isGameOver())
                    {
                        textView.append("GAME OVER!!!");
                    }
                    else
                    {
                        board.minimax(0,1);
                        board.placeAMove(board.AI_move,1);
                        int m = board.AI_move.i;
                        int n = board.AI_move.j;

                        b[m][n].setEnabled(false);
                        b[m][n].setText("X");
                        info.append("\nAI TURN: " + m + "," + n);
                    }



                }

                if(board.isGameOver()) {

                    if (board.hasXWon())
                        textView.setText("AI WON");
                    else if (board.hasOWon())
                        textView.setText("You WON");
                    else
                        textView.setText("IT'S A DRAW");
                }


            }



        }
    }











}

class Game_Board
{

    //X ->1 and O->0. and empty->2
    int[][] board;
    int xSize, ySize;

    public Game_Board(int xSize, int ySixe)
    {
        board = new int[xSize][ySixe];
        this.xSize = xSize;
        this.ySize = ySixe;

        for(int i=0; i< xSize; i++)
        {
            for(int j =0; j< ySixe; j++)
            {
                board[i][j] = 0;
            }
        }

    }

    public  void restart()
    {
        for(int i=0; i< xSize; i++)
        {
            for(int j =0; j< ySize; j++)
            {
                board[i][j] = 0;
            }

        }


    }

    public boolean isGameOver()
    {

        return (hasXWon() || hasOWon() || getAvailableStates().isEmpty());
    }

    public boolean hasXWon() {
        if ((board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] == 1) || (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[0][2] == 1)) {
            //System.out.println("X Diagonal Win");
            return true;
        }
        for (int i = 0; i < 3; ++i) {
            if (((board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] == 1)
                    || (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] == 1))) {
                // System.out.println("X Row or Column win");
                return true;
            }
        }
        return false;
    }

    public boolean hasOWon() {
        if ((board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[0][0] == 2) || (board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[0][2] == 2)) {
            // System.out.println("O Diagonal Win");
            return true;
        }
        for (int i = 0; i < 3; ++i) {
            if ((board[i][0] == board[i][1] && board[i][0] == board[i][2] && board[i][0] == 2)
                    || (board[0][i] == board[1][i] && board[0][i] == board[2][i] && board[0][i] == 2)) {
                //  System.out.println("O Row or Column win");
                return true;
            }
        }

        return false;
    }


    public List<BoardLocation> getAvailableStates() {
        List<BoardLocation> availablePoints = new ArrayList<>();
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                if (board[i][j] == 0) {
                    availablePoints.add(new BoardLocation(i, j));
                }
            }
        }
        return availablePoints;
    }

    public void placeAMove(BoardLocation point, int player)
    {
        board[point.i][point.j] = player;
    }

    BoardLocation AI_move;

    public int minimax(int depth, int turn)
    {
        if (hasXWon()) return +1;
        if (hasOWon()) return -1;

        List<BoardLocation> pointsAvailable = getAvailableStates();
        if (pointsAvailable.isEmpty()) return 0;

        int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;

        for (int i = 0; i < pointsAvailable.size(); ++i) {
            BoardLocation point = pointsAvailable.get(i);
            if (turn == 1)
            {
                placeAMove(point, 1);
                int currentScore = minimax(depth + 1, 2);
                max = Math.max(currentScore, max);

                if(depth == 0)System.out.println("Score for position "+(i+1)+" = "+currentScore);
                if(currentScore >= 0)
                {
                    if(depth == 0)
                        AI_move = point;
                }
                if(currentScore == 1){board[point.i][point.j] = 0; break;}
                if(i == pointsAvailable.size()-1 && max < 0)
                {
                    if(depth == 0)
                        AI_move = point;
                }
            } else if (turn == 2)
            {
                placeAMove(point, 2);
                int currentScore = minimax(depth + 1, 1);
                min = Math.min(currentScore, min);
                if(min == -1){board[point.i][point.j] = 0; break;}
            }
            board[point.i][point.j] = 0; //Reset this point
        }
        return turn == 1?max:min;
    }


}

class BoardLocation
{
    int i;
    int j;

    public BoardLocation(int i, int j) {
        this.i = i;
        this.j = j;
    }


}

class PointOnBoardLocation
{
    int point;
    BoardLocation location;

    PointOnBoardLocation( int point,BoardLocation location)
    {
        this.point = point;
        this.location = location;
    }

}

