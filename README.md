# TicTacToe #
This is a simple TicTacToe game developed for android. The main concept of this game is that the user can select to which grid he wants to play in. There are two opponents, the computer and the user. The computer ALWAYS wins.

# License #

```
#!python

    TicTacToe
    Copyright (C) 2017 Ali Murad

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
```